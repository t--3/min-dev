# -*- mode: ruby -*-
# vi: set ft=ruby :

if Vagrant::Util::Platform.windows? then
    unless Vagrant.has_plugin?("vagrant-winnfsd")
        system "vagrant plugin install vagrant-winnfsd"
    end
end
unless Vagrant.has_plugin?("vagrant-bindfs")
    system "vagrant plugin install vagrant-bindfs"
end
unless Vagrant.has_plugin?("vagrant-hostmanager")
    system "vagrant plugin install vagrant-hostmanager"
end
unless Vagrant.has_plugin?("vagrant-triggers")
    system "vagrant plugin install vagrant-triggers"
end

Vagrant.configure("2") do |config|
    # Base configuration
    config.vm.box = "ArminVieweg/ubuntu-xenial64-lamp"

    staticIpAddress = "192.168.29.192"
    httpPortForwardingHost = "8484"
    config.vm.hostname = "ext-min.vagrant"
    
    if Vagrant::Util::Platform.windows? then
        config.trigger.after :up, :good_exit => [0, 1] do
            run "explorer http://#{config.vm.hostname}"
        end
    end

	config.ssh.insert_key = false

    config.vm.network "private_network", type: "dhcp"
    config.vm.provider "virtualbox" do |vb|
        vb.memory = 4096
        vb.cpus = 2
    end

    # Synchronization
    config.vm.synced_folder ".", "/vagrant", disabled: true
    config.vm.synced_folder ".", "/var/nfs", type: "nfs"

    config.bindfs.bind_folder "/var/nfs", "/vagrant",
        perms: "u=rwX:g=rwX:o=rD"

    config.bindfs.bind_folder "/var/nfs/min", "/var/www/min",
            perms: "u=rwX:g=rwX:o=rD", force_user: "vagrant", force_group: "www-data"

    # Hostmanager
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true

    config.vm.define "default" do |node|
        node.vm.network :private_network, ip: staticIpAddress
        node.vm.network :forwarded_port, guest: 80, host: httpPortForwardingHost
    end

    # Provider Scripts

    # Run once (Basic setup)
    config.vm.provision "shell", run: "once", name: "basic-setup", inline: <<-SHELL
        cd /var/www/html
        cp -Rf /vagrant/html/* .
        chown -R vagrant .
        chgrp -R www-data .

        printf "\ncd /var/www" >> /home/vagrant/.bashrc
    SHELL

    # Run once (TYPO3 7 LTS)
    config.vm.provision "shell", run: "once", name: "typo3-7-install", inline: <<-SHELL
        mkdir /var/www/html7
        echo -e "Alias /7 \"/var/www/html7/public/\"\n<Directory \"/var/www/html7/public/\">\nOrder allow,deny\nAllow from all\nRequire all granted\n</Directory>" > /etc/apache2/conf-available/html7.conf
        a2enconf html7
        service apache2 restart

        cd /var/www/html7

        cp -Rf /vagrant/html_typo3_7/* .

        echo "Fetching dependencies (this may take a while)..."
        composer install --no-progress

        echo "Replacing min extension with symlink..."
        rm -Rf /var/www/html7/public/typo3conf/ext/min
        ln -s /var/www/min /var/www/html7/public/typo3conf/ext/min

        echo "Installing TYPO3 CMS..."
        vendor/bin/typo3cms install:setup --force --database-user-name "root" --database-user-password "" --database-host-name "localhost" --database-name "typo3_7" --database-port "3306" --database-socket "" --admin-user-name "admin" --admin-password "password" --site-name "T3 Min Dev Environment" --site-setup-type "site" --use-existing-database 0
        vendor/bin/typo3cms cache:flush

        if [ -f "dump.sql" ]
        then
        	echo "Importing sql dump..."
        	composer run sql-import
        fi

        echo "Fixing permissions..."
        chmod 2775 . ./public/typo3conf ./public/typo3conf/ext
        chown -R vagrant .
        chgrp -R www-data .

        echo "TYPO3 7.6 successfully installed!"
    SHELL

    # Run once (TYPO3 8 LTS)
    config.vm.provision "shell", run: "once", name: "typo3-8-install", inline: <<-SHELL
        mkdir /var/www/html8
        echo -e "Alias /8 \"/var/www/html8/public/\"\n<Directory \"/var/www/html8/public/\">\nOrder allow,deny\nAllow from all\nRequire all granted\n</Directory>" > /etc/apache2/conf-available/html8.conf
        a2enconf html8
        service apache2 restart

        cd /var/www/html8

        cp -Rf /vagrant/html_typo3_8/* .

        echo "Fetching dependencies (this may take a while)..."
        composer install --no-progress

        echo "Replacing min extension with symlink..."
        rm -Rf /var/www/html8/public/typo3conf/ext/min
        ln -s /var/www/min /var/www/html8/public/typo3conf/ext/min

        echo "Installing TYPO3 CMS..."
        vendor/bin/typo3cms install:setup --force --database-user-name "root" --database-user-password "" --database-host-name "localhost" --database-name "typo3_8" --database-port "3306" --database-socket "" --admin-user-name "admin" --admin-password "password" --site-name "T3 Min Dev Environment" --site-setup-type "site" --use-existing-database 0
        vendor/bin/typo3cms cache:flush

        if [ -f "dump.sql" ]
        then
        	echo "Importing sql dump..."
        	composer run sql-import
        fi

        echo "Fixing permissions..."
        chmod 2775 . ./public/typo3conf ./public/typo3conf/ext
        chown -R vagrant .
        chgrp -R www-data .

        echo "TYPO3 8.7 successfully installed!"
    SHELL

    # Run once (TYPO3 9 LTS)
    config.vm.provision "shell", run: "once", name: "typo3-9-install", inline: <<-SHELL
        mkdir /var/www/html9
        echo -e "Alias /9 \"/var/www/html9/public/\"\n<Directory \"/var/www/html9/public/\">\nOrder allow,deny\nAllow from all\nRequire all granted\n</Directory>" > /etc/apache2/conf-available/html9.conf
        a2enconf html9
        service apache2 restart

        cd /var/www/html9

        cp -Rf /vagrant/html_typo3_9/* .

        echo "Fetching dependencies (this may take a while)..."
        composer install --no-progress

        echo "Replacing min extension with symlink..."
        rm -Rf /var/www/html9/public/typo3conf/ext/min
        ln -s /var/www/min /var/www/html9/public/typo3conf/ext/min

        echo "Installing TYPO3 CMS..."
        vendor/bin/typo3cms install:setup --force --database-user-name "root" --database-user-password "" --database-host-name "localhost" --database-name "typo3_9" --database-port "3306" --database-socket "" --admin-user-name "admin" --admin-password "password" --site-name "T3 Min Dev Environment" --site-setup-type "site" --use-existing-database 0
        vendor/bin/typo3cms cache:flush

        if [ -f "dump.sql" ]
        then
        	echo "Importing sql dump..."
        	composer run sql-import
        fi

        echo "Fixing permissions..."
        chmod 2775 . ./public/typo3conf ./public/typo3conf/ext
        chown -R vagrant .
        chgrp -R www-data .

        echo "TYPO3 9 successfully installed!"
    SHELL

    # Run once (Add /adminer alias)
    config.vm.provision "shell", run: "once", name:"adminer-install", inline: <<-SHELL
        echo "Installing adminer..."
        composer require vrana/adminer -d /home/vagrant/.composer/ -o --no-progress
        ln -s /home/vagrant/.composer/vendor/vrana/adminer/adminer /var/www/adminer

        echo -e "Alias /adminer \"/var/www/adminer/\"\n<Directory \"/var/www/adminer/\">\nOrder allow,deny\nAllow from all\nRequire all granted\n</Directory>" > /etc/apache2/conf-available/adminer.conf
        a2enconf adminer
        echo "Restarting apache2..."
        service apache2 restart
    SHELL

    # Run always
    config.vm.provision "shell", run: "always", name: "composer-update", inline: <<-SHELL
        cd ~
        composer self-update --no-progress

        touch /var/www/html7/public/typo3conf/ENABLE_INSTALL_TOOL
        touch /var/www/html8/public/typo3conf/ENABLE_INSTALL_TOOL
        touch /var/www/html9/public/typo3conf/ENABLE_INSTALL_TOOL
    SHELL
end
