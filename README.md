# Dev Environment for EXT:min

This repository provides scripts, data and configuration
to setup a full TYPO3 CMS development environment for the extension ["min"](https://bitbucket.org/t--3/min).

Supported versions:

- TYPO3 7.6 LTS
- TYPO3 8.7 LTS
- TYPO3 9


## Components

* Vagrantfile to provide easy to set up dev environment
* composer.json with [typo3/minimal](https://packagist.org/packages/typo3/minimal) configuration
* Submodule to actual main extension (located in `/min`)
* AdditionalConfiguration.php for debug setup (located in `/html_typo3_x/public/typo3conf/AdditionalConfiguration.php`)
* MySQL dump (gets imported automatically on provisioning of vagrant box)


## Requirements

* Virtual Box
* Vagrant
* Git

## Installation

1. Clone this repository instead of the extension repo. It contains the extension
using a git submodule in folder `/min`. To fetch also the submodules use:

    ```
    git clone --recurse-submodules https://bitbucket.org/t--3/min-dev.git
    ```

2. Run `vagrant up`. That's it.


### Credentials

* Instances
    * Overview - http://ext-min.vagrant
    * TYPO3 7.6 LTS - http://ext-min.vagrant/7
    * TYPO3 8.7 LTS - http://ext-min.vagrant/8
    * TYPO3 9 - http://ext-min.vagrant/9
    * Adminer - http://ext-min.vagrant/adminer
 
* TYPO3 Backend
    * Username: `admin`
    * Password: `password` (also for install tool)

* More:
    * MySQL login: `root` / *no password*
    * Vagrant SSH login: `vagrant` / `vagrant` (or `vagrant ssh`on CLI)

For MySQL the client [Adminer](https://www.adminer.org) is also installed and available under:
http://ext-min.vagrant/adminer 


### Mappings

The **min** folder is synced to `/var/www/min` and within TYPO3 installations it is embedded 
with symlinks. When you also want to sync installation related files (like root composer.json 
for a TYPO3 instance), you need to map the following local paths, to remote:

| Local          | Remote           |
| -------------- | ---------------- |
| `html_typo3_7` | `/var/www/html7` |
| `html_typo3_8` | `/var/www/html8` |
| `html_typo3_9` | `/var/www/html9` |
| `html`         | `/var/www/html`  |
| `min`          | `/var/www/min`   |
